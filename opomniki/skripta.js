window.addEventListener('load', function() {
	//stran nalozena

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			if(cas == 0) {
				window.alert("Opomnik!\n\nZadolžitev " + opomnik.querySelector('.naziv_opomnika').innerHTML + " je potekla!");
				opomnik.parentNode.removeChild(opomnik);
			}
			else {
				cas = cas - 1;
				casovnik.innerHTML = cas.toString();
			}
			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	
	setInterval(posodobiOpomnike, 1000);

	document.querySelector('#prijavniGumb').addEventListener('click', function() {
	
    	document.querySelector('#uporabnik').innerHTML = document.querySelector('#uporabnisko_ime').value;
	
		document.querySelector('.pokrivalo').style.visibility = "hidden";
	
	});
	
	document.querySelector('#dodajGumb').addEventListener('click', function() {
	
		//Pridobi podatke o imenu in času
    	var nazivOpominka = document.querySelector('#naziv_opomnika').value;
    	var casTrajanja = document.querySelector('#cas_opomnika').value;
    	
    	//Pobriše polje po vnosu
    	document.querySelector('#naziv_opomnika').value = "";
		document.querySelector('#cas_opomnika').value = "";
		
		//Ustvari nov "opomnik"
		var opom = document.createElement('div');
		//in mu določi html
		opom.innerHTML =	"<div class='opomnik rob senca'>\
								<div class='naziv_opomnika'></div>\
								<div class='cas_opomnika'> Opomnik čez <span></span> sekund.</div>\
							</div>";


		//Opomink doda okvirju z opomniki
		document.querySelector('#opomniki').appendChild(opom);
		
		//Posodobi vrednosti v opomnkiku z vneseno vrednostijo
		opom.querySelector('.naziv_opomnika').innerHTML = nazivOpominka;
		opom.querySelector("span").innerHTML = casTrajanja;
	});

});


